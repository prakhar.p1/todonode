const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(morgan("combined"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
module.exports = app;